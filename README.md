# Find RSS feeds for railwaygazette.com

## What?!

Railway Gazette is a website for railway news.

They do not link to their RSS feeds anywhere and I had difficulties finding them, so I wrote a script that finds all feeds for me.

I noticed that feed URLs follow this pattern:

https://www.railwaygazette.com/NUMBER.rss

Not all numbers contain feeds, most are empty, so I decided to just try them all out.

It's a very simple script. The code could probably be shorter, but I did not bother to keep it readable, it's just a few lines anyway.

## How?!

Requirements:

```
bash
xmlstarlet
echo
```

Please note that you need a version of echo that supports `-n` to not output newlines. If you have a different version, then you need to change the corresponding parts.

Just run it: `./railway-gazette.sh`

It will write its output to `railway-gazette-rss-feeds.csv` in the same directory.

## Haha, I can do that better!!11!!

Merge Requests that make this script work on more Operating Systems like FreeBSD or OpenBSD are welcome.

I would also like to see the dependency on versions of `echo` that support `-n` removed. Maybe we can also do it without `xmlstarlet` and just with built-in tools of the shell?
