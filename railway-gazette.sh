#!/usr/bin/env bash

for NUMBER in {0..1000000}; do # I know there are feeds around 1200, but not sure how far up this goes, so let's find out
    echo -n $NUMBER
    # To let you know what feed number I just checked, no line break so we can add a bingo to the line if we find anything 

    OUTPUTRSS=$(curl --silent https://www.railwaygazette.com/$NUMBER.rss)
    # Load output of feed into variable to do stuff with that

    grep --quiet --max-count 1 "<pubDate>" <<< $OUTPUTRSS && echo -n "https://www.railwaygazette.com/$NUMBER.rss" >> railway-gazette-rss-feeds.csv
    # Turns out that the website returns feeds for all numbers, but only real feeds have "<pubDate>" in them and others are empty, so if we save the URL if it contains anything

    grep --quiet --max-count 1 "<pubDate>" <<< $OUTPUTRSS && echo -n "," >> railway-gazette-rss-feeds.csv
    # If the URL is valid, then add a comma to create a CSV

    grep --quiet --max-count 1 "<pubDate>" <<< $OUTPUTRSS && xmlstarlet sel -t -m "/rss/channel[1]" -v "title" >> railway-gazette-rss-feeds.csv <<< $OUTPUTRSS
    # Add title to CSV for readability

    grep --quiet --max-count 1 "<pubDate>" <<< $OUTPUTRSS && echo "" >> railway-gazette-rss-feeds.csv
    # If the URL is valid, then add a line break before the next line

    grep --quiet --max-count 1 "<pubDate>" <<< $OUTPUTRSS || echo ""
    grep --quiet --max-count 1 "<pubDate>" <<< $OUTPUTRSS && echo " - Bingo!"
    #curl --silent https://www.railwaygazette.com/$NUMBER.fullrss | grep "<pubDate>" && echo "https://www.railwaygazette.com/$NUMBER.fullrss" >> railway-gazette-rss-feeds.csv

    sleep 1
    # Should avoid rate limiting, yes it is slow now... But maybe there is none? Did not notice any rate limiting after 5000 feeds...

done
